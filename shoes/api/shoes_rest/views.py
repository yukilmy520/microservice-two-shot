from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


from common.json import ModelEncoder
from .models import BinVO, Shoes

# Create your views here.
class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "id",
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href"
    ]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "model_name",
        "manufacturer",
        "picture_url",
        "color",
        "id"
        ]
    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name }

class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "model_name",
        "manufacturer",
        "picture_url",
        "color",
        ]
    enconders = {
        "bin": BinVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
            return JsonResponse(
                {"shoes": shoes},
                encoder=ShoesListEncoder,
            )
        else:
            shoes = Shoes.objects.all()
            return JsonResponse(
                {"shoes": shoes},
                encoder=ShoesListEncoder,
            )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            #bin_href = f'/api/bins/{bin_vo_id}'
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request, id):
    if request.method == "GET":
        try:
            shoes = Shoes.objects.get(id=id)
            return JsonResponse(
                shoes,
                encoder=ShoesDetailEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid shoe"},
                status=400,
            )
    else:# request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    # else:
    #     content = json.loads(request.body)
    #     try:
    #         if "bin" in content:
    #             bin = BinVO.objects.get(id=content.bin.id)
    #             content["bin"] = bin
    #     except BinVO.DoesNotExist:
    #         return JsonResponse(
    #             {"message": "Invalid bin id"},
    #             status=400,
    #         )

    #     Shoes.objects.filter(id=id).update(**content)

    #     shoes = Shoes.objects.get(id=id)
    #     return JsonResponse(
    #         shoes,
    #         encoder=ShoesDetailEncoder,
    #         safe=False,
    #     )
