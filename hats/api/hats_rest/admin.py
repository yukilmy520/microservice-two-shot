from django.contrib import admin
from .models import Hat

@admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    )
