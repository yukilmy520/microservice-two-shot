import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
# from shoes_rest.models import Something
from hats_rest.models import LocationVO


def get_location():
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)
    for loc in content["locations"]:
        LocationVO.objects.update_or_create(
            import_href=loc["href"],
            closet_name=loc["closet_name"],
            section_number=loc["section_number"],
            shelf_number=loc["shelf_number"],
            defaults={"closet_name": loc["closet_name"]},
        )

def poll():
    while True:
        print('Hats poller polling for data')
        try:
            # Write your polling logic, here
            get_location()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
