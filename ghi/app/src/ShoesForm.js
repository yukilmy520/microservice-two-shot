import React from 'react';

class ShoesForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      model_name: '',
      manufacturer: '',
      color: '',
      picture_url: '',
      bin:'',
      bins: []
    };
    this.handleModelNameChange = this.handleModelNameChange.bind(this);
    this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handlePictureChange = this.handlePictureChange.bind(this);
    this.handleBinsChange = this.handleBinsChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.bins;
    console.log(data)


    const binUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(binUrl, fetchConfig);
    if (response.ok) {
      const newBin = await response.json();

      const cleared = {
        model_name: '',
        manufacturer: '',
        color: '',
        picture_url: '',
        bin:'',
      };
      this.setState(cleared);
    }
  }

  handleModelNameChange(event) {
    const value = event.target.value;
    this.setState({model_name: value})
  }

  handleManufacturerChange(event) {
    const value = event.target.value;
    this.setState({manufacturer: value})
  }

  handleColorChange(event) {
    const value = event.target.value;
    this.setState({color: value})
  }

  handlePictureChange(event) {
    const value = event.target.value;
    this.setState({picture_url: value})
  }

  handleBinsChange(event) {
    const value = event.target.value;
    this.setState({bin: value});
  }

  async componentDidMount() {
    const url = `http://localhost:8100/api/bins/`;

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({bins: data.bins});
      console.log(data)
    }
  }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoes</h1>
            <form onSubmit={this.handleSubmit} id="create-shoes-form">
              <div className="form-floating mb-3">
                <input value={this.state.model_name} onChange={this.handleModelNameChange} placeholder="model_name" required type="text" name="model_name" id="model_name" className="form-control" />
                <label htmlFor="model_name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.manufacturer} onChange={this.handleManufacturerChange} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.color} onChange={this.handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.picture_url} onChange={this.handlePictureChange} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture Url</label>
              </div>
              <div className="mb-3">
                <select value={this.state.bin} onChange={this.handleBinsChange} required name="bin" id="bin" className="form-select">
                  <option value="">Choose a bin</option>
                  {this.state.bins.map(bin => {
                    return (
                      <option key={bin.id} value={bin.href}>
                        {bin.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ShoesForm;
