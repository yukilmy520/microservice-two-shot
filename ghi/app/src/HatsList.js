import React from "react";

function HatsList(props) {
    const deleteHat = async (href) => {
        fetch(`http://localhost:8090/${href}`, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json'
          }
        }).then(() => {
          window.location.reload();
      })
      }
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Picture</th>
            <th>Name</th>
            <th>Fabric</th>
            <th>Style Name</th>
            <th>Color</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map(hat => {
            return (
              <tr key={hat.href}>
                <td ><img src={hat.picture_url} alt="HatImage.txt" width="20%" height="20%"/></td>
                <td>{ hat.name }</td>
                <td >{ hat.fabric }</td>
                <td >{ hat.style_name }</td>
                <td >{ hat.color }</td>
                <td >{ hat.location }</td>
                <td><button onClick={() => deleteHat(hat.href)} type="button" className="btn btn-dark">Delete</button></td>
              </tr>

            );
          })}
        </tbody>
      </table>
    );
  }

  export default HatsList;
