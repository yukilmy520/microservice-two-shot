# Wardrobify

Team:

* Person 1 - Which microservice?
Yuki(Minyue) Lu
* Person 2 - Which microservice?
Graham.Monnig


## Design
## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

My Shoes model contains the shoe resource to track its manufacturer, its model name, its color, a URL for a picture, and the bin in the wardrobe where it exists. I set up the poller to pull the bin information from the wardrobe microservice. After finished the back-end set up, I built a React component to fetch the list, show the list of shoes, and submit form in the Web page.


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

Creates an object that has values for Name, Fabric, Style Type, Color, a Picture URL, and the Location.
Can create a new object through the Hats API backend as well as through the React frontend
Can be deleted through the Hats API backend as well as through the React frontend
Interacts with the Wardrobe API by pulling Location into a Value Object
Mainly interacts with the React Frontend as the Hat Database


## Step-by-step instructions to run the project:
1.git clone this project to your local repository;
2.run the docker by typing "dock-compose build" and "docker-compose up" commands;
3.to access the front-end react, we go to "localhost:3000" to see all the listings of shoes and hats, and create/delete new shoes and hats;
4.to access the back-end Hats api, use localhost:8090 and use the provided URLS to interact with the DB;
5.to access the back-end Shoes api, use localhost:8080 and use the provided URLS to interact with the DB;
6.to access the back-end Wardrobe api, use localhost:8100 and use the provided URLS to interact with the DB.


## Diagram:
![Diagram](image.png)


## URLs and ports for each of the services:
service: wardrobe-api
ports:
      - "8100:8000"
urls: http://localhost:8100


service: react
ports:
      - "3000:3000"
urls:https://localhost:3000


service: shoes-api
ports:
      - "8080:8000"
urls:https://localhost:8080


service: hats-api:
ports:
      - "8090:8000"
urls:https://localhost:8090


## CRUD Route Documentation for each of the services:
service: shoes-api
| Method | URL | What it does |
| --- | --- | --- |
| GET    | /api/bins/<int:bin_vo_id>/shoes/ | Gets a list of all shoes for a specific bin|
| GET    | /api/shoes/<int:id>/             | Gets the details of one instance of shoes  |
| POST   | /api/shoes/                      | Create a new instance of shoes             |
| DELETE | /api/shoes/<int:id>/             | Deletes a single instance of shoes         |

service: hats-api
| Method | URL | What it does |
| --- | --- | --- |
| GET    | /api/location/<int:location_vo_id>/hats/ | Gets a list of all hats for a specific location|
| GET    | /api/hats/<int:id>/                      | Gets the details of one instance of hats       |
| POST   | /api/hats/                               | Create a new instance of hat                   |
| DELETE | /api/hats/<int:id>/                      | Deletes a single instance of hat               |


## Value Objects:
* bins: manufacturer, model name, color, picture url, bin
* locations: fabric, style name, color, picture, location
